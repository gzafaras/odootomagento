/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.odootomagento;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Paint;


/**
 * FXML Controller class
 *
 * @author gzafaras
 */
public class MainFrameController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        stop.setDisable(true);
    }

    @FXML
    Label statusLabel;
    @FXML
    TextArea interresults, errorresults;
    @FXML
    Button syncBrands;
    @FXML
    Button syncOrders;
    @FXML
    Button syncBrandgroups;
    @FXML
    Button stop;
    @FXML
    ProgressBar progressBar;
//    SyncBrands sync;
//    SyncOrders syncOrd;
//    List<Brands> brands;
//    SalesOrderListEntityArray pendingOrders;
    CategoriesController syncCategories;
    ProductController syncProducts;
    private Service<Void> backroundService;

    @FXML
    private void syncBrandsclick(ActionEvent event) {
//        syncBrands.setDisable(true);
//        syncOrders.setDisable(true);
//        stop.setDisable(false);
//        statusLabel.setTextFill(Paint.valueOf("blue"));
//        statusLabel.setText("Ξ”ΞΉΞ±ΟƒΞΉΞΊΞ±ΟƒΞ―Ξ± Ξ£Ο…Ξ½Ξ³Ο‡ΟΞΏΞ½ΞΉΟƒΞΌΞΏΟ");
try {
            syncProducts = new ProductController();

        } catch (Exception ex) {
            ex.printStackTrace();
            statusLabel.setTextFill(Paint.valueOf("red"));
            statusLabel.setText("Sync Problem\n");

        }

        backroundService = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {

                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
//                       
                        StringBuilder text = new StringBuilder();
                        final StringBuilder texterr = new StringBuilder();
                        
                        ResultSet products = syncProducts.getOdooProducts();
                        int size = syncProducts.countOdooProudcts();
                        int count = 0;
                        String result;
                        text.append("Process start\n");
                        updateMessage(text.toString());
                        syncProducts.createSession();
                        while(products.next())
                        {
                            if (isCancelled()) {
                                updateMessage("Process cancel by user");
                                return null;
                            }
                            if(products.getInt("magento_id") == 0)
                            {
                                result = syncProducts.createProduct(products);
                                
                                if(result.contains("#"))
                                    texterr.append(result);
                                else
                                    text.append(result);
                                count +=1;
                            }else
                            {
                                result = syncProducts.updateProduct(products);
                                
                                if(result.contains("#"))
                                    texterr.append(result);
                                else
                                    text.append(result);
                                count +=1;
                                
                            }
                             updateMessage(text.toString());
                             updateProgress(count, size);
                             Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {

                                                interresults.setScrollTop(Double.MAX_VALUE);
                                            }
                                        });
                             Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            
                                            errorresults.setText(texterr.toString());
                                            errorresults.setScrollTop(Double.MAX_VALUE);
                                        }
                                    });
                        }
                        syncProducts.destroySession();
                        
                        
                        
                        
                        

                        try {
                            Thread.sleep(2);
                        } catch (InterruptedException interrupted) {
                            if (isCancelled()) {
                                updateMessage("Process cancel by user");

                                return null;
                            }
                        } catch (Exception ex) {

                            return null;
                        }

                        return null;
                    }

                    @Override
                    protected void failed() {
                        super.failed();
                        updateMessage("Ξ ΟΟΞ²Ξ»Ξ·ΞΌΞ± ΟƒΟ„Ξ·Ξ½ Ξ΄ΞΉΞ±Ξ΄ΞΉΞΊΞ±ΟƒΞ―Ξ± ΟƒΟ…Ξ½Ξ³Ο‡ΟΞΏΞ½ΞΉΟƒΞΌΞΏΟ\n");

                    }
                };

            }
        };

        backroundService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {

                syncBrands.setDisable(false);
                syncOrders.setDisable(false);
                stop.setDisable(true);

                interresults.textProperty().unbind();

                syncProducts.destroySession();
            }
        });

        backroundService.setOnCancelled(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {

                syncCategories.getSyncCategories().getService().destroySession();
                syncOrders.setDisable(false);
                syncBrands.setDisable(false);
                stop.setDisable(true);
                statusLabel.setTextFill(Paint.valueOf("red"));
                statusLabel.setText("Ξ”ΞΉΞ±ΞΊΞΏΟ€Ξ® Ξ•ΟΞ³Ξ±ΟƒΞ―Ξ±Ο‚");

            }
        });
        interresults.textProperty().bind(backroundService.messageProperty());
        //errorresults.textProperty().bind(backroundService.titleProperty());
        progressBar.progressProperty().bind(backroundService.progressProperty());
        backroundService.restart();    }

    @FXML
    private void syncOrdersClick(ActionEvent event) {
//        syncBrands.setDisable(true);
//        syncOrders.setDisable(true);
//        stop.setDisable(false);
//        statusLabel.setTextFill(Paint.valueOf("blue"));
//        statusLabel.setText("Ξ”ΞΉΞ±ΟƒΞΉΞΊΞ±ΟƒΞ―Ξ± Ξ£Ο…Ξ½Ξ³Ο‡ΟΞΏΞ½ΞΉΟƒΞΌΞΏΟ");
//        try {
//            syncOrd = new SyncOrders();
//            pendingOrders = syncOrd.getPendingOrders();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            statusLabel.setTextFill(Paint.valueOf("red"));
//            statusLabel.setText("Ξ ΟΟΞ²Ξ»Ξ·ΞΌΞ± ΞΊΞ±Ο„Ξ¬ Ο„Ξ·Ξ½ Ξ΄ΞΉΞ±Ξ΄ΞΉΞΊΞ±ΟƒΞ―Ξ± Ο„ΞΏΟ… ΟƒΟ…Ξ½Ο‡Ξ³ΟΞΏΞ½ΞΉΟƒΞΌΞΏΟ\n");
//
//        }
//
//        backroundService = new Service<Void>() {
//            @Override
//            protected Task<Void> createTask() {
//
//                return new Task<Void>() {
//                    @Override
//                    protected Void call() throws Exception {
//
//                        Integer size = new Integer(0);
//                        try {
//                            size = pendingOrders.getComplexObjectArray().size();
//                            //System.out.println(size);
//                        } catch (Exception ex) {
//                            ex.printStackTrace();
//                        }
//                        Double count = new Double(0);
//
//                        StringBuilder text = new StringBuilder();
//
//                        for (SalesOrderListEntity order : pendingOrders.getComplexObjectArray()) {
//                            if (isCancelled()) {
//                                break;
//
//                            }
//                            try {
//                                text.append(order.getIncrementId() + "\n");
//                                updateMessage(text.toString());
//                                text.append("\n");
//                                updateMessage(text.toString());
//
//                                if (order.getIncrementId().trim().equals("144901159") || order.getIncrementId().trim().equals("134495449") || order.getIncrementId().trim().equals("121845200") || order.getIncrementId().trim().equals("124993122") || order.getIncrementId().trim().equals("127569432") || order.getIncrementId().trim().equals("113665122")) {
//                                    syncOrd.createOrder(order);
//
//                                }
//
//                            } catch (Exception ex) {
//                            }
//                            ++count;
//                            updateProgress(count, size);
//
//                            try {
//                                Thread.sleep(2);
//                            } catch (InterruptedException interrupted) {
//                                if (isCancelled()) {
//                                    updateMessage("Ξ”ΞΉΞ±ΞΊΞΏΟ€Ξ® Ξ•ΟΞ³Ξ±ΟƒΞ―Ξ±Ο‚");
//
//                                    break;
//                                }
//                            } catch (Exception ex) {
//
//                                break;
//                            }
//                        }
//
//                        return null;
//                    }
//
//                    @Override
//                    protected void failed() {
//                        super.failed();
//                        updateMessage("Ξ ΟΟΞ²Ξ»Ξ·ΞΌΞ± ΟƒΟ„Ξ·Ξ½ Ξ΄ΞΉΞ±Ξ΄ΞΉΞΊΞ±ΟƒΞ―Ξ± ΟƒΟ…Ξ½Ξ³Ο‡ΟΞΏΞ½ΞΉΟƒΞΌΞΏΟ\n");
//
//                    }
//                };
//
//            }
//        };
//
//        backroundService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
//            @Override
//            public void handle(WorkerStateEvent event) {
//                statusLabel.setTextFill(Paint.valueOf("green"));
//                statusLabel.setText("ΞΞ»ΞΏΞΊΞ»Ξ®ΟΟ‰ΟƒΞ· Ξ•ΟΞ³Ξ±ΟƒΞ―Ξ±Ο‚");
//                syncBrands.setDisable(false);
//                syncOrders.setDisable(false);
//                stop.setDisable(true);
//
//                interresults.textProperty().unbind();
//                interresults.appendText("ΞΞ»ΞΏΞΊΞ»Ξ®ΟΟ‰ΟƒΞ· Ξ”ΞΉΞ±Ξ΄ΞΉΞΊΞ±ΟƒΞ―Ξ±Ο‚\n");
//                syncOrd.endSync();
//
//            }
//        });
//
//        backroundService.setOnCancelled(new EventHandler<WorkerStateEvent>() {
//            @Override
//            public void handle(WorkerStateEvent event) {
//
//                syncOrd.endSync();
//                syncOrders.setDisable(false);
//                syncBrands.setDisable(false);
//                stop.setDisable(true);
//                statusLabel.setTextFill(Paint.valueOf("red"));
//                statusLabel.setText("Ξ”ΞΉΞ±ΞΊΞΏΟ€Ξ® Ξ•ΟΞ³Ξ±ΟƒΞ―Ξ±Ο‚");
//
//            }
//        });
//        interresults.textProperty().bind(backroundService.messageProperty());
//        progressBar.progressProperty().bind(backroundService.progressProperty());
//        backroundService.restart();

    }

    @FXML
    private void syncBrandGroupsClick(ActionEvent event) throws SQLException {
        syncBrands.setDisable(true);
        syncOrders.setDisable(true);
        stop.setDisable(false);
        statusLabel.setTextFill(Paint.valueOf("blue"));
        statusLabel.setText("Start Action");
        try {
            syncCategories = new CategoriesController();

        } catch (Exception ex) {
            ex.printStackTrace();
            statusLabel.setTextFill(Paint.valueOf("red"));
            statusLabel.setText("Sync Problem\n");

        }

        backroundService = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {

                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
//                       
                        StringBuilder text = new StringBuilder();
                        StringBuilder texterr = new StringBuilder();
                        
                        ResultSet categories = syncCategories.getCategories();
                        int size = syncCategories.getSize();
                        int count = 0;
                        String result;
                       syncCategories.setInterResult("Sync process start!!\n");
                        updateMessage(syncCategories.getInterResult().toString());
                        
                        while(categories.next())
                        {
                            System.out.println(categories.getInt("id"));
                            if(categories.getInt("magento_id") == -1)
                            {
                                result = syncCategories.getSyncCategories().createCategory(categories);
                                if(result.contains("#"))
                                    syncCategories.setErrorResults(result);
                                else
                                    syncCategories.setInterResult(result);
                                count +=1;
                            }else
                            {
                                result = syncCategories.getSyncCategories().updateCategory(categories);
                                
                                if(result.contains("#"))
                                    syncCategories.setErrorResults(result);
                                else
                                    syncCategories.setInterResult(result);
                                count +=1;
                                System.out.println(syncCategories.getInterResult());
                            }
                             updateMessage(syncCategories.getInterResult().toString());
                             updateProgress(count, size);
                             Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {

                                                interresults.setScrollTop(Double.MAX_VALUE);
                                            }
                                        });
                             Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            
                                            errorresults.setText(syncCategories.getErrorResults().toString());
                                            errorresults.setScrollTop(Double.MAX_VALUE);
                                        }
                                    });
                        }
                        
                        
                        
                        
                        

                        try {
                            Thread.sleep(2);
                        } catch (InterruptedException interrupted) {
                            if (isCancelled()) {
                                updateMessage("Ξ”ΞΉΞ±ΞΊΞΏΟ€Ξ® Ξ•ΟΞ³Ξ±ΟƒΞ―Ξ±Ο‚");

                                return null;
                            }
                        } catch (Exception ex) {

                            return null;
                        }

                        return null;
                    }

                    @Override
                    protected void failed() {
                        super.failed();
                        updateMessage("Ξ ΟΟΞ²Ξ»Ξ·ΞΌΞ± ΟƒΟ„Ξ·Ξ½ Ξ΄ΞΉΞ±Ξ΄ΞΉΞΊΞ±ΟƒΞ―Ξ± ΟƒΟ…Ξ½Ξ³Ο‡ΟΞΏΞ½ΞΉΟƒΞΌΞΏΟ\n");

                    }
                };

            }
        };

        backroundService.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {

                syncBrands.setDisable(false);
                syncOrders.setDisable(false);
                stop.setDisable(true);

                interresults.textProperty().unbind();

                syncCategories.getSyncCategories().getService().destroySession();

            }
        });

        backroundService.setOnCancelled(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {

                syncCategories.getSyncCategories().getService().destroySession();
                syncOrders.setDisable(false);
                syncBrands.setDisable(false);
                stop.setDisable(true);
                statusLabel.setTextFill(Paint.valueOf("red"));
                statusLabel.setText("Ξ”ΞΉΞ±ΞΊΞΏΟ€Ξ® Ξ•ΟΞ³Ξ±ΟƒΞ―Ξ±Ο‚");

            }
        });
        interresults.textProperty().bind(backroundService.messageProperty());
        //errorresults.textProperty().bind(backroundService.titleProperty());
        progressBar.progressProperty().bind(backroundService.progressProperty());
        backroundService.restart();

    }

    @FXML
    public void stopSync(ActionEvent event) {
        backroundService.cancel();
    }

}

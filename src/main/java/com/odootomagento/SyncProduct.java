package com.odootomagento;

import Connection.OdooConnection;
import Connection.MagentoConnection;
import Connection.MagentoServiceConnect;
import com.odootomagento.magento.ArrayOfString;
import com.odootomagento.magento.CatalogProductCreateEntity;
import com.odootomagento.magento.CatalogProductCreateRequestParam;
import com.odootomagento.magento.CatalogProductDeleteRequestParam;
import com.odootomagento.magento.CatalogProductUpdateRequestParam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class SyncProduct {

    private OdooConnection odoo;
    private MagentoConnection mage;
    private MagentoServiceConnect service;

    public SyncProduct(OdooConnection odoo, MagentoConnection mage) {

        this.odoo = odoo;
        this.mage = mage;
        this.service = new MagentoServiceConnect("magentoservice.prop");
    }
    public int getOdooProductCount() throws SQLException
    {
        String sql = "select coalesce(count(*),0) aa  from product_product";
        Statement productStmt;
        ResultSet odooProducts = null;
        try {
            productStmt = odoo.createConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            odooProducts = productStmt.executeQuery(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        odooProducts.first();
        return odooProducts.getInt("aa");
        
    }

    public ResultSet getOdooProductsForCreate() {
        
        Statement productStmt;
        String sql = "select pr.id pid,coalesce(pr.default_code,pr.name_template) sku,pr.name_template pname,coalesce(pr.magento_id,0)magento_id,"
                + "coalesce(pr.weight,0) weight,pt.list_price price,ph.cost,coalesce(atax.amount,0),coalesce(pc.magento_id,0)catmagento_id "
                + "from product_product pr "
                + "left join product_template pt on pt.id = pr.product_tmpl_id "
                + "left join product_price_history ph  on ph.product_id = pr.id "
                + "left join product_taxes_rel tax on tax.prod_id = pr.id "
                + "left join product_category pc on pc.id = pt.categ_id "
                + "left join account_tax   atax on atax.id = tax.tax_id ";

        ResultSet odooProducts = null;
        try {
            productStmt = odoo.createConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            odooProducts = productStmt.executeQuery(sql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return odooProducts;

    }

    public String createMagentoProduct(ResultSet rs) throws SQLException {
        Connection odoocon = null;
        String session = service.getSession();

        String result = "";
        CatalogProductCreateRequestParam createProductParameters = new CatalogProductCreateRequestParam();

        createProductParameters.setSessionId(session);
        createProductParameters.setSku(rs.getString("sku"));
        createProductParameters.setSet("4");
        createProductParameters.setStore("0");
        createProductParameters.setType("simple");

        CatalogProductCreateEntity productData = new CatalogProductCreateEntity();
        //add category
        ArrayOfString category = new ArrayOfString();
        category.getComplexObjectArray().add(rs.getString("catmagento_id"));
        productData.setCategoryIds(category);
        productData.setVisibility("4");
        productData.setStatus("1");
        productData.setDescription(rs.getString("pname"));
        productData.setShortDescription(rs.getString("pname"));
        productData.setUrlKey(rs.getString("sku"));
        productData.setUrlPath(rs.getString("sku") + ".html");
        productData.setName(rs.getString("pname"));
        productData.setOptionsContainer("container1");
        productData.setPrice(rs.getBigDecimal("price").toString());
        productData.setWeight(rs.getBigDecimal("weight").toString());

        createProductParameters.setProductData(productData);
        int product_id = 0;
        try {
            product_id = service.getPort().catalogProductCreate(createProductParameters).getResult();
            odoocon = this.odoo.createConnection();
            odoocon.setAutoCommit(false);
            String sql = "update product_product set magento_id = ? where id = ? ";
            PreparedStatement updateOdooProduct = odoocon.prepareStatement(sql);
            updateOdooProduct.setInt(1, product_id);
            updateOdooProduct.setInt(2, rs.getInt("pid"));
            updateOdooProduct.executeUpdate();
            odoocon.commit();
            result = "The product with sku " + rs.getString("sku") + " created successfully!!\n";
        } catch (Exception ex) {
            ex.printStackTrace();
            if(product_id != 0)
            {
                CatalogProductDeleteRequestParam deleteparameter = new CatalogProductDeleteRequestParam();
                deleteparameter.setProductId(Integer.toString(product_id));
                deleteparameter.setSessionId(session);
                service.getPort().catalogProductDelete(deleteparameter);
                
            }
            result = "#Error in create action of product " + rs.getString("sku")+"\n";
            if (odoocon != null) {
                try {
                    System.err.println("Problem in create action the odoo transaction is rolled back");
                    System.err.println(ex.getMessage());
                    odoocon.rollback();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            odoocon.setAutoCommit(true);
            if (odoocon != null) {
                odoocon.close();
            }
        }
        return result;
    }
    
    public String updateMagentoProduct(ResultSet rs) throws SQLException {
        Connection odoocon = null;
        String session = service.getSession();

        String result = "";
        CatalogProductUpdateRequestParam updateProductParameters = new CatalogProductUpdateRequestParam();

        updateProductParameters.setSessionId(session);
        
        
        updateProductParameters.setStore("0");
        updateProductParameters.setProductId(Integer.toString(rs.getInt("magento_id")));

        CatalogProductCreateEntity productData = new CatalogProductCreateEntity();
        //add category
        ArrayOfString category = new ArrayOfString();
        category.getComplexObjectArray().add(rs.getString("catmagento_id"));
        productData.setCategoryIds(category);
        productData.setVisibility("4");
        productData.setStatus("1");
        productData.setDescription(rs.getString("pname"));
        productData.setShortDescription(rs.getString("pname"));
        productData.setUrlKey(rs.getString("sku"));
        productData.setUrlPath(rs.getString("sku") + ".html");
        productData.setName(rs.getString("pname"));
        productData.setOptionsContainer("container1");
        productData.setPrice(rs.getBigDecimal("price").toString());
        productData.setWeight(rs.getBigDecimal("weight").toString());

        updateProductParameters.setProductData(productData);
        boolean updated = false;
        try {
            updated = service.getPort().catalogProductUpdate(updateProductParameters).isResult();
            if(updated)
                result = "The product with sku " + rs.getString("sku") + " updated successfully!!\n";
            else
                result = "#Error in update action of product " + rs.getString("sku")+"\n";
        } catch (Exception ex) {
                result = "#Error in update action of product " + rs.getString("sku")+"\n";
            
        } 
        return result;
    }

    public void createIntAttributes(Long id, Connection con) throws SQLException {
        String sql;

        Statement insertIntAttr = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        sql = "select * from catalog_product_entity_int";
        ResultSet insertAttrRs = insertIntAttr.executeQuery(sql);
        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 96);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateInt("value", 1);
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 102);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateInt("value", 4);
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 121);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateInt("value", 2);
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 100);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateInt("value", 0);
        insertAttrRs.insertRow();

    }

    public void createDecimalAttributes(Long id, ResultSet rs, Connection con) throws SQLException {
        String sql;

        Statement insertIntAttr = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        sql = "select * from catalog_product_entity_decimal";
        ResultSet insertAttrRs = insertIntAttr.executeQuery(sql);
        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 75);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateBigDecimal("value", rs.getBigDecimal("price"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 80);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateBigDecimal("value", rs.getBigDecimal("weight"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 74);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);

        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 120);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);

        insertAttrRs.insertRow();

    }

    public void createVarcharlAttributes(Long id, ResultSet rs, Connection con) throws SQLException {
        String sql;

        Statement insertIntAttr = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        sql = "select * from catalog_product_entity_varchar";
        ResultSet insertAttrRs = insertIntAttr.executeQuery(sql);
        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 71);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("pname"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 82);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("pname"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 84);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("pname"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 85);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", "no_selection".trim());
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 86);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", "no_selection".trim());
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 87);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", "no_selection".trim());
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 97);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("sku"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 98);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("sku") + ".html");
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 98);
        insertAttrRs.updateInt("store_id", 1);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("sku") + ".html");
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 75);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateBigDecimal("value", rs.getBigDecimal("price"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 103);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 107);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 109);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", "container1");
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 117);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 118);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", "2");
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 119);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", "4");
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 122);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.insertRow();
    }

    public void createTextAttributes(Long id, ResultSet rs, Connection con) throws SQLException {
        String sql;

        Statement insertIntAttr = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        sql = "select * from catalog_product_entity_text";
        ResultSet insertAttrRs = insertIntAttr.executeQuery(sql);
        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 72);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("pname"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 73);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.updateString("value", rs.getString("pname"));
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 83);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.insertRow();

        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("entity_type_id", 4);
        insertAttrRs.updateInt("attribute_id", 106);
        insertAttrRs.updateInt("store_id", 0);
        insertAttrRs.updateLong("entity_id", id);
        insertAttrRs.insertRow();

    }

    public MagentoServiceConnect getService() {
        return service;
    }

    public void CreateCategoryProcuct(Long id, ResultSet rs, Connection mage) throws SQLException {
        String sql;

        Statement insertIntAttr = mage.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        sql = "select * from catalog_category_product";
        ResultSet insertAttrRs = insertIntAttr.executeQuery(sql);
        insertAttrRs.moveToInsertRow();
        insertAttrRs.updateInt("category_id", rs.getInt("catmagento_id"));
        insertAttrRs.updateLong("product_id", id);
        insertAttrRs.insertRow();

    }

}

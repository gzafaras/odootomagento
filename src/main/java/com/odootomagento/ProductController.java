package com.odootomagento;

import Connection.OdooConnection;
import Connection.MagentoConnection;
import java.sql.ResultSet;
import java.sql.SQLException;



public class ProductController {
	private SyncProduct syncBrands;
        

	public ProductController()  {
		
		this.syncBrands = new SyncProduct(new OdooConnection("odoo.prop"), new MagentoConnection("magento.prop"));
		
                
		
		//this.syncBrands.createMagentoProduct(syncBrands.getOdooProductsForCreate());
	}

        
public void createSession()
{
    syncBrands.getService().createSession();
}        
public void destroySession()
{
    syncBrands.getService().destroySession();
}

public String createProduct(ResultSet rs) throws SQLException
{
    return syncBrands.createMagentoProduct(rs);
}

public String updateProduct(ResultSet rs) throws SQLException
{
    return syncBrands.updateMagentoProduct(rs);
}

public ResultSet getOdooProducts()
{
    return syncBrands.getOdooProductsForCreate();
}
public int countOdooProudcts() throws SQLException
{
    return syncBrands.getOdooProductCount();
}

	
}

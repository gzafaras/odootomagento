package com.odootomagento;

import Connection.OdooConnection;
import Connection.MagentoConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CategoriesController {
	private SyncCategories syncCategories;
        private StringBuilder interResult,errorResults;
        private int size;
        ResultSet categories;
	public CategoriesController() throws SQLException {
		
		syncCategories = new SyncCategories(new OdooConnection("odoo.prop"), new MagentoConnection("magento.prop"));
                size =  syncCategories.countOdooCategories();
                categories = syncCategories.OdooCategories();
                syncCategories.getService().createSession();
                interResult = new StringBuilder();
                errorResults = new StringBuilder();
                String result;
                
//            try {
//                while(categories.next())
//                {
//                    if(categories.getInt("magento_id") == -1){
//                        result = this.syncCategories.createCategory(categories);
//                        if(result.contains("#"))
//                            errorResults.append(result);
//                        else
//                            interResult.append(result);
//                    
//                    }
//                    else{
//                        result = this.syncCategories.updateCategory(categories);
//                        if(result.contains("#"))
//                            errorResults.append(result);
//                        else
//                            interResult.append(result);
//                    }
//                    
//                }
//                syncCategories.getService().destroySession();
//            } catch (SQLException ex) {
//                Logger.getLogger(CategoriesController.class.getName()).log(Level.SEVERE, null, ex);
//            }
		
		
	}

    public StringBuilder getInterResult() {
        return interResult;
    }

    public StringBuilder getErrorResults() {
        return errorResults;
    }

    public int getSize() {
        return size;
    }

    public ResultSet getCategories() {
        return categories;
    }

    public void setErrorResults(String errorResults) {
        this.errorResults.append(errorResults);
    }

    public void setInterResult(String interResult) {
        this.interResult.append(interResult+"\n");
    }

    public SyncCategories getSyncCategories() {
        return syncCategories;
    }
    
    
    
    
    
    
    
        
        
	

}

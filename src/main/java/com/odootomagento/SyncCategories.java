package com.odootomagento;

import Connection.OdooConnection;
import Connection.MagentoConnection;
import Connection.MagentoServiceConnect;
import com.odootomagento.magento.ArrayOfString;
import com.odootomagento.magento.CatalogCategoryCreateRequestParam;
import com.odootomagento.magento.CatalogCategoryEntityCreate;
import com.odootomagento.magento.CatalogCategoryUpdateRequestParam;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SyncCategories {

    private OdooConnection odoo;
    private MagentoConnection mage;
    private MagentoServiceConnect service;

    public SyncCategories(OdooConnection odoo, MagentoConnection mage) {

        this.odoo = odoo;
        this.mage = mage;
        service = new MagentoServiceConnect("magentoservice.prop");
    }

    public MagentoServiceConnect getService() {
        return service;
    }

    public ResultSet OdooCategories() {
        ResultSet odooCategories = null;
        Statement odooCategoriesstmt = null;
        try {
            odooCategoriesstmt = this.odoo.createConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String getCategories = "WITH RECURSIVE nodes_cte(id, parent_id, depth, path,pathn,name,magento_id) AS (\n"
                    + " SELECT tn.id,tn.parent_id, 1::INT AS depth, tn.name::TEXT AS path,tn.id::Text as pathn,tn.name ,magento_id\n"
                    + " FROM product_category AS tn \n"
                    + " WHERE tn.parent_id IS NULL\n"
                    + "UNION ALL\n"
                    + " SELECT c.id, c.parent_id, p.depth + 1 AS depth, \n"
                    + "        (p.path || '/' || c.name),(p.pathn || '/' || c.id::TEXT),c.name,c.magento_id \n"
                    + " FROM nodes_cte AS p, product_category AS c \n"
                    + " WHERE c.parent_id = p.id\n"
                    + ")\n"
                    + "SELECT id,coalesce(parent_id,-1)parent_id,depth,path,pathn,name,coalesce(magento_id,-1) magento_id FROM nodes_cte AS n  ORDER BY n.id ASC;";
            odooCategories = odooCategoriesstmt.executeQuery(getCategories);
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return odooCategories;
    }
    
    public int  countOdooCategories() throws SQLException {
        ResultSet odooCategories = null;
        Statement odooCategoriesstmt = null;
        try {
            odooCategoriesstmt = this.odoo.createConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String getCategories = "select coalesce(count(*),0) num from product_category";
            odooCategories = odooCategoriesstmt.executeQuery(getCategories);
        } catch (SQLException e) {

            e.printStackTrace();
        }
        odooCategories.first();
        return odooCategories.getInt("num");
    }

    public String createCategory(ResultSet category) throws SQLException {
        String session = service.getSession();
        Connection odoo = this.odoo.createConnection();
        odoo.setAutoCommit(false);
        //System.out.println(session);
        CatalogCategoryCreateRequestParam catalogparmeters = new CatalogCategoryCreateRequestParam();
        catalogparmeters.setSessionId(session);

        catalogparmeters.setStore("0");
        if (category.getInt("parent_id") != -1) {
            Statement parent = null;
            parent = odoo.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            String parentSql = "select magento_id from product_category where id = " + category.getInt("parent_id");
            ResultSet parentRs = parent.executeQuery(parentSql);
            if (parentRs.first()) {
                catalogparmeters.setParentId(parentRs.getInt("magento_id"));
            }
        } else {
            catalogparmeters.setParentId(2);
        }
        CatalogCategoryEntityCreate categoryData = new CatalogCategoryEntityCreate();

        categoryData.setName(category.getString("name"));
        categoryData.setIsActive(1);
        categoryData.setUrlKey(category.getString("name"));
        categoryData.setIsAnchor(1);
        categoryData.setIncludeInMenu(1);
        categoryData.setDescription(category.getString("name"));
        categoryData.setPosition(1);
        ArrayOfString array = new ArrayOfString();
        array.getComplexObjectArray().add("position");
        categoryData.setAvailableSortBy(array);
        categoryData.setDefaultSortBy("position");
        catalogparmeters.setCategoryData(categoryData);
        int magento_id = 0;
        String result;
        try {
            magento_id = service.getPort().catalogCategoryCreate(catalogparmeters).getResult();
            String updatebrandgroups = "update product_category set magento_id = " + magento_id + " where id = " + category.getInt("id");
            Statement updateBrandgroups = odoo.createStatement();
            updateBrandgroups.executeUpdate(updatebrandgroups);
            odoo.commit();
            result = "The category " + category.getString("name") + " created succssesfully!\n";
;
        } catch (Exception ex) {
            ex.printStackTrace();
            if (odoo != null) {
                try {
                    System.err.print("Transaction odoo is being rolled back!");
                    odoo.rollback();
                } catch (SQLException e) {
                    
                    e.printStackTrace();
                }
            }
            result = "#Problem with category " + category.getString("name") + "!\n";
        } finally {

            odoo.setAutoCommit(true);
            if (odoo != null) {
                odoo.close();
            }
        }

        return result;
    }
    
     public String updateCategory(ResultSet category) throws SQLException {
        String session = service.getSession();
        Connection odoo = this.odoo.createConnection();
        
        //System.out.println(session);
         CatalogCategoryUpdateRequestParam catalogparmeters = new CatalogCategoryUpdateRequestParam();
        catalogparmeters.setSessionId(session);
        catalogparmeters.setCategoryId(category.getInt("magento_id"));
        catalogparmeters.setStore("0");
        
        CatalogCategoryEntityCreate categoryData = new CatalogCategoryEntityCreate();
        
        categoryData.setName(category.getString("name"));
        categoryData.setIsActive(1);
        categoryData.setUrlKey(category.getString("name"));
        categoryData.setIsAnchor(1);
        categoryData.setIncludeInMenu(1);
        categoryData.setDescription(category.getString("name"));
        categoryData.setPosition(1);
        ArrayOfString array = new ArrayOfString();
        array.getComplexObjectArray().add("position");
        categoryData.setAvailableSortBy(array);
        categoryData.setDefaultSortBy("position");
        catalogparmeters.setCategoryData(categoryData);
        boolean updated = false;
        String result = "";
        try {
            updated = service.getPort().catalogCategoryUpdate(catalogparmeters).isResult();
            
            if(updated)
                result = "The category " + category.getString("name") + " updated succssesfully!\n";
    
            else
                result ="#Problem in update action in category with name " + category.getString("name") + "\n!";
        } catch (Exception ex) {
            ex.printStackTrace();
                result ="#Problem in update action in category with name " + category.getString("name") + "!\n";
        } finally {

            
            if (odoo != null) {
                odoo.close();
            }
        }

        return result;

}
     
     
}
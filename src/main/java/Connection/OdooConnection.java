package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OdooConnection  extends DbConnection{

	
	public OdooConnection(String filename) {
		super(filename);
		// TODO Auto-generated constructor stub
	}

	@Override
	public java.sql.Connection createConnection() {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection con = null;
		try {
			con = DriverManager.getConnection(
			   "jdbc:postgresql://"+getHost()+":"+getPort()+"/"+getDatabase(),getUsername(),getPassword());
			setConnection(con);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return getConnection();
	}

	

	

}

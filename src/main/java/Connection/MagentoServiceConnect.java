package Connection;

import com.odootomagento.magento.EndSessionParam;
import com.odootomagento.magento.LoginParam;
import com.odootomagento.magento.MagentoService;
import com.odootomagento.magento.PortType;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;



public class MagentoServiceConnect {
	private String session,username = "",password = "",filename;
	private PortType port;
        private MagentoService ws;
        private LoginParam login;
        
    public void readCradentials()
    {
        try {
			BufferedReader in = new BufferedReader(new FileReader(this.filename));
			String line;
			while ((line = in.readLine()) != null) {
				String[] tmp = line.split(":");
				String prop = tmp[0];
				switch (prop) {
				case "username":
					this.username = tmp[1];

					break;

				case "password":
					this.password = tmp[1];

					break;
				
				default:
					break;
				}

			}
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

    public MagentoServiceConnect(String filename) {
        this.filename = filename;
        readCradentials();
        login = new LoginParam();
        ws = new MagentoService();
        port = ws.getPort();
        login.setUsername(username);
        login.setApiKey(password);
        
    }
    public void createSession()
    {
        this.session = port.login(login).getResult();
    }
    
    public boolean destroySession()
    {
     
        EndSessionParam parameters = new EndSessionParam();
        parameters.setSessionId(this.session);
        return port.endSession(parameters).isResult();
    
    }

    public String getSession() {
        return session;
    }

    public PortType getPort() {
        return port;
    }
    
    
    
    
    
    
    
        

}





package Connection;

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class DbConnection implements ManageConnection {
	Connection connection = null;
	private String username;
	private String password;
	private String database;
	private String host;
	private String port;
	private String serverZone = null;
	private String prefix ="";

	public DbConnection(String filename) {

		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = in.readLine()) != null) {
				String[] tmp = line.split(":");
				String prop = tmp[0];
				switch (prop) {
				case "username":
					this.username = tmp[1];

					break;

				case "password":
					this.password = tmp[1];

					break;
				case "host":
					this.host = tmp[1];

					break;
				case "port":
					this.port = tmp[1];

					break;
				case "database":
					this.database = tmp[1];

					break;
				case "serverTimezone":
					this.serverZone = tmp[1];

					break;
				case "prefix":
					this.prefix = tmp[1];

					break;

				default:
					break;
				}

			}
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getDatabase() {
		return database;
	}

	public String getHost() {
		return host;
	}

	public String getPort() {
		return port;
	}

	public Connection getConnection() {
		return connection;
	}

	public String getServerZone() {
		return serverZone;
	}

	public void setServerZone(String serverZone) {
		this.serverZone = serverZone;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
	
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String toString() {
		return "Connection [username=" + username + ", password=" + password + ", database=" + database + ", host="
				+ host + ", port=" + port + "]";
	}

}

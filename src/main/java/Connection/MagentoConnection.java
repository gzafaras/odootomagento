package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class MagentoConnection extends DbConnection {

	public MagentoConnection(String filename) {
		super(filename);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public Connection createConnection() {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection con = null;
		try {
			StringBuilder jdbc = new StringBuilder("jdbc:mysql://"+getHost()+"/"+getDatabase()+"?" +
                    "user="+getUsername()+"&password="+getPassword());
			if(getServerZone() != null )
				jdbc.append("&serverTimezone="+getServerZone());
			con = DriverManager.getConnection(jdbc.toString());
			setConnection(con);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return getConnection();
	}


}
